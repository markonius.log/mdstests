import React from 'react'
import { render, fireEvent, getByTestId } from '@testing-library/react'
import App from './App'
import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

describe('<App />', () => {
  let wrapper
  const setState = jest.fn()
  const useStateSpy = jest.spyOn(React, 'useState')
  useStateSpy.mockImplementation(init => [init, setState])

  beforeEach(() => {
    wrapper = Enzyme.mount(<App />)
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  describe('sign in', () => {
    // it('should not sign in', () => {
    //   wrapper.find('#usernameInput').props().onChange('username')
    //   // wrapper.find('#passwordInput').props()
    //   wrapper.find('#signInButton').props().onClick()
    //   expect(
    //     wrapper.find('#status').props().isConnected
    //   ).toEqual(false)
    // })

    it('initial state', () => {
      const { container } = render(<App />)
      const username = getByTestId(container, 'usernameInput')
      expect(username.textContent).toBe('')
      
      const password = getByTestId(container, 'passwordInput')
      expect(password.textContent).toBe('')

      const status = getByTestId(container, 'status')
      expect(status.textContent).toBe('Disconnected')
    }),

    it('reject sign in', () => {
      const { container } = render(<App />)
      const username = getByTestId(container, 'usernameInput')
      const password = getByTestId(container, 'passwordInput')
      const button = getByTestId(container, 'signInButton')

      fireEvent.change(username, {target: {value: 'admin'}})
      fireEvent.change(password, {target: {value: 'wrongPassword'}})
      fireEvent.click(button)
      
      const status = getByTestId(container, 'status')
      expect(status.textContent).toBe('Disconnected')
    }),

    it('accept sign in', () => {
      const { container } = render(<App />)
      const username = getByTestId(container, 'usernameInput')
      const password = getByTestId(container, 'passwordInput')
      const button = getByTestId(container, 'signInButton')

      const newUsername = 'admin'
      fireEvent.change(username, {target: {value: newUsername}})
      expect(username.value).toBe(newUsername)
      fireEvent.change(password, {target: {value: 'root'}})
      fireEvent.click(button)
      
      const status = getByTestId(container, 'status')
      expect(status.textContent).toBe('Connected')
    })
  })
})