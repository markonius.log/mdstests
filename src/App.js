import React, { useState } from 'react'
import { Input } from 'antd'
import { Row, Col } from 'antd'
import { Button } from 'antd'
import { Typography } from 'antd'
import { notification } from 'antd'

import logo from './logo.svg'
import './App.css'
import 'antd/dist/antd.css'

const { Text } = Typography

function Status(props) {
  const { isConnected } = props

  if ( isConnected ) {
    return(
      <Text style={{color: 'lightGreen'}}  data-testid='status' >Connected</Text>
    )
  } else {
    return (
      <Text style={{color: 'white'}}  data-testid='status' >Disconnected</Text>
    )
  }
}

function App() {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [isConnected, setIsConnected] = React.useState(false)
  const [isClicked, setIsClicked] = React.useState(false)

  const onSignIn = () => {
    setIsClicked(true)

    if ( !isConnected ) {
      if (username === 'admin' && password === 'root') {
        notification['success']({
          message: 'Signed In',
          description: 'Successfully signed in.'
        })
        setIsConnected(true)
      } else {
        notification['error']({
          message: 'Error while trying to sign in',
          description: 'Credentials do not match. Please check your inputs.'
        })
      }
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Row style={{width: '100%'}}>
          <Col span={4} offset={10}>
            <Status isConnected={isConnected} isClicked={isClicked} />

            <Input 
              style={{marginBottom: '1em'}}
              placeholder='Username'
              value={username}
              onChange={event => setUsername(event.target.value)}
              onKeyPress={event => {if ( event.key === 'Enter' ) {onSignIn()}}}
              disabled={isConnected}
              name='username'
              data-testid='usernameInput'
            />

            <Input 
              style={{marginBottom: '1em'}}
              placeholder='Password'
              value={password}
              onChange={event => setPassword(event.target.value)}
              onKeyPress={event => {if ( event.key === 'Enter' ) {onSignIn()}}}
              disabled={isConnected}
              type='password'
              name='password'
              data-testid='passwordInput'
            />
            <Button onClick={onSignIn} disabled={isConnected} data-testid='signInButton' >Sign in</Button>
          </Col>
        </Row>
      </header>
    </div>
  )
}

export default App
